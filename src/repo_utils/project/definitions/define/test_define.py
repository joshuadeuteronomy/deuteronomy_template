from define import define
import os
import shutil
import pathlib

curr_dir = pathlib.Path(__file__).parent.absolute()


def test_define():
    name = 'test_def'
    name_dir = os.path.join(curr_dir, 'test_def')
    # Remove test directory if it's already there
    try:
        shutil.rmtree(name_dir)
    except Exception:
        pass

    define(name_dir)
    # Folder
    assert os.path.exists(name_dir)
    # Definition file
    assert os.path.exists(os.path.join(name_dir, name+".py"))
    # Test file
    assert os.path.exists(os.path.join(name_dir, "test_"+name+".py"))

    # Remove test directory
    shutil.rmtree(name_dir)

def test_define_path():
    name_dir = os.path.join(curr_dir, 'test_def')
    # Remove test directory if it's already there
    try:
        shutil.rmtree(name_dir)
    except Exception:
        pass

    define(name_dir)
    # Folder
    assert os.path.exists(name_dir)
    # Definition file
    assert os.path.exists(os.path.join(name_dir, "test_def.py"))
    # Test file
    assert os.path.exists(os.path.join(name_dir, "test_test_def.py"))

    # Remove test directory
    shutil.rmtree(name_dir)
