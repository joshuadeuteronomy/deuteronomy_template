from def_from_dir import def_from_dir
import git

def test_def_from_dir():
    repo = git.Repo(__file__, search_parent_directories=True)
    repo_root = repo.working_tree_dir
    func = def_from_dir(f'{repo_root}/src/functions/project/definitions/define')
