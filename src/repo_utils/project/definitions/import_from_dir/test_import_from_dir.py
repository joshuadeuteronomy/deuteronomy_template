from repo_utils import import_from_dir, find_definition, get_repository_path
from os.path import join

def test_import_same_dir():
    mod = import_from_dir(join(get_repository_path(), 'src', 'functions'))
    assert mod