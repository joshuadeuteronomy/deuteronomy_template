"""
Solutions define interfaces that may be implemented in various ways over time. Solutions may be any type of Python variable or value: functions, classes, objects, constants, etc.

A standard init file is used from src.
"""
from os import path
src_init_path = path.abspath(path.join(path.dirname(path.realpath(__file__)), '..', '__init__namespace.py'))
exec(open(src_init_path).read())