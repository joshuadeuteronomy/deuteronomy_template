"""
All definitions are imported under their namespace directory.
All directories one level below src are considered namespace directories.
"""
import os
from os.path import join
import git
import glob
from importlib.util import spec_from_file_location, module_from_spec
import sys


repo_path = os.path.dirname(os.path.realpath(__file__))
repo_name = __name__

def import_from_dir(path: str):
    """ Given a path to a directory with an __init__.py file, return the module """
    name = os.path.basename(path)
    mod = None
    try:
        path = join(path,'__init__.py')
        spec = spec_from_file_location(name, path)
        mod = module_from_spec(spec)
        spec.loader.exec_module(mod)
        mod = getattr(mod, name)
    except (ModuleNotFoundError, AttributeError):
        pass
    return mod


def import_all_spaces(root_mod_name, space_level: int=1):
    """ Import all space modules in the repository under src """
    repo = git.Repo(__file__, search_parent_directories=True)
    repo_root = repo.working_tree_dir
    src_root = join(repo_root, 'src')
    defs = {}
    # Find directories at the space level
    level_path = ['*' for _ in range(space_level)]
    spaces_path = join(src_root, *level_path)
    spaces_files = glob.glob(spaces_path)
    spaces_dirs = filter(lambda f: not f.startswith('_') and os.path.isdir(f), spaces_files)
    for space_file in spaces_files:
        space_name = os.path.basename(space_file)
        if not space_name.startswith('_') and os.path.isdir(space_file):
            space_mod = import_from_dir(space_file)
            sys.modules[f'{root_mod_name}.{space_name}'] = space_mod
            sys.modules[space_name] = space_mod
            defs[space_name] = space_mod
    return defs


# Find all definitions right now by importing all the top-level
# directories under src.
definitions = import_all_spaces(root_mod_name=repo_name)
